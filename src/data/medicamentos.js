export default medicamentos = [
  {
    medicamento : "Acetaminofen",
    dosisDia: 60,
    dosisAlDia: 4,
    concentracion: 30,
    resultado : "cc c/6h"
  },
  {
    medicamento : "Amoxicilina",
    dosisDia: 80,
    dosisAlDia: 3,
    concentracion: 50,
    resultado : "cc c/8h"
  },
  {
    medicamento : "Albendazol",
    dosisDia: 400,
    dosisAlDia: 1,
    concentracion: 40,
    resultado : "cc c/dia"
  },
  {
    medicamento : "Cefradina",
    dosisDia: 100,
    dosisAlDia: 4,
    concentracion: 50,
    resultado : "cc c/6h"
  },
  {
    medicamento : "Cefalexina",
    dosisDia: 100,
    dosisAlDia: 4,
    concentracion: 50,
    resultado : "cc c/6h"
  },
  {
    medicamento : "Clorfeniramin",
    dosisDia: 0.4,
    dosisAlDia: 4,
    concentracion: 0.5,
    resultado : "cc c/6h u 8h"
  },
  {
    medicamento : "Dicloxacilina",
    dosisDia: 50,
    dosisAlDia: 4,
    concentracion: 50,
    resultado : "cc c/6h "
  },
  {
    medicamento : "DIPIRONA",
    dosisDia: 40,
    dosisAlDia: 3,
    concentracion: 0.4,
    resultado : "cc c/6h"
  },
  {
    medicamento : "ERITROMICINA",
    dosisDia: 40,
    dosisAlDia: 4,
    concentracion: 50,
    resultado : "cc c/6h "
  },
  {
    medicamento : "Hidroxicina",
    dosisDia: 2,
    dosisAlDia: 4,
    concentracion: 2.5,
    resultado : "cc c/6h "
  },
  {
    medicamento : "KETOTIFENO",
    dosisDia: 0.05,
    dosisAlDia: 1,
    concentracion: 0.4,
    resultado : "cc c/12h "
  },
  {
    medicamento : "LORATADINA",
    dosisDia: 0.45,
    dosisAlDia: 3,
    concentracion: 5,
    resultado : "cc c/8h "
  },
  {
    medicamento : "Metocloprami",
    dosisDia: 50,
    dosisAlDia: 3,
    concentracion: 50,
    resultado : "cc c/8h "
  },
  {
    medicamento : "METRONIDAZOL",
    dosisDia: 15,
    dosisAlDia: 3,
    concentracion: 25,
    resultado : "cc c/8h "
  },
  {
    medicamento : "Naproxeno",
    dosisDia: 150000,
    dosisAlDia: 3,
    concentracion: 100000,
    resultado : "cc c/8h "
  },
  {
    medicamento : "Nistatina",
    dosisDia: 50,
    dosisAlDia: 4,
    concentracion: 100,
    resultado : "cc c/6h "
  },
  {
    medicamento : "OXACILINA",
    dosisDia: 50,
    dosisAlDia: 4,
    concentracion: 100,
    resultado : "cc c/6h "
  },
]