import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { AutomaticScreen, ManualScreen, HomeScreen, SemanaGestacional } from './screens';





const Stack = createNativeStackNavigator();

const  MyStack = () => {
  return (
    <Stack.Navigator  
    
    screenOptions={{
      
      headerStyle: {
        backgroundColor: 'purple',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
      headerTitleAlign: 'center',
    }}
    >
        
      <Stack.Screen  options={{headerShown:false}}  name="HomeScreen" component= {HomeScreen}/>
      <Stack.Screen name="Automatico" component={AutomaticScreen} />
      <Stack.Screen name="Manual" component={ManualScreen} />
      <Stack.Screen name="SemanaGestacional" component={SemanaGestacional} />
    </Stack.Navigator>
  );
}


export const Navigation = () => {

  return (
    
        <MyStack/>
   
  )
}
