import React , {useState} from 'react'
import { View , TextInput, StyleSheet, TouchableOpacity,Text} from 'react-native';
import SelectDropdown from 'react-native-select-dropdown'
import medicamentos from "../data/medicamentos"

export const AutomaticScreen = () => {
  const [peso, setPeso] = useState()
  const [medicamentoElegido, setMedicamentoElegido] = useState("")
  const [dosis, setDosis] = useState(0)

 const calcularDosis = () => {
  if (medicamentoElegido != "" && peso != "" ) {
    let parametros = (medicamentos.find(medicamento => medicamento.medicamento== medicamentoElegido));
      setDosis((((parametros.dosisDia*peso)/parametros.concentracion))/parametros.dosisAlDia)
      return console.log("se hizo el calculo");
  }
    return console.log("no se pudo calcular");
 }
  
  return (
    <View style={{
        flex: 1,
        alignItems: 'center',
        top:110,
    }}>
      <Text  style={styles.textIndication}> Elige el medicamento</Text>
      <SelectDropdown  
      dropdownStyle={{
        borderRadius : 10,
      }}
      buttonStyle={styles.dropdown }
      buttonTextStyle={styles.buttonText}
          data={medicamentos.map((medicamento) => medicamento.medicamento )}

          defaultButtonText="Elige el Medicamento"
          onSelect={(selectedItem, index) => {
            setMedicamentoElegido(selectedItem)
          }}
          buttonTextAfterSelection={(selectedItem, index) => {
            // text represented after item is selected
            // if data array is an array of objects then return selectedItem.property to render after item is selected
            return selectedItem
          }}
          rowTextForSelection={(item, index) => {
            // text represented for each item in dropdown
            // if data array is an array of objects then return item.property to represent item in dropdown
            return item
          }}
        />
        <Text style={styles.textIndication} > Ingrese2 el peso del paciente</Text>
        <TextInput
        style={styles.input}
        onChangeText={(e) => setPeso(e)}
        defaultValue=""
        value={peso}
        placeholder="Peso"
        keyboardType="numeric"
        inputMode='numeric'
        
        />
      <TouchableOpacity
          style={styles.button}
          onPress={calcularDosis}
        >
          <Text style={styles.buttonText} > Calcular</Text>
      </TouchableOpacity>
      <Text  style={styles.resutaldo}>{dosis} -  {(medicamentos.find(medicamento => medicamento.medicamento== medicamentoElegido))?.resultado}</Text>
    </View>

    
  )
}


const styles = StyleSheet.create({

  dropdown:{
    width: '60%',
    height: 50,
    backgroundColor: 'blue',
    borderRadius: 10,
    marginVertical: 10,
  },
  input: {
    height: 40,
    margin: 15,
    borderWidth: 1,
    padding: 10,
    alignItems: "flex-end",
  },
  resutaldo : {
    fontSize : 25,
  }, 
  button: {
    width: '60%',
    height: 50,
    backgroundColor: 'blue',
    borderRadius: 10,
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
  },
  textIndication : {
    fontSize: 18,
    fontWeight: 'bold',
  }
});