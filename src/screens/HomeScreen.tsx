import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
// import { AdMobBanner } from 'expo-ads-admob';

const screens =["Manual", "Automatico", "SemanaGestacional"] 

export const HomeScreen = () => {
  const navigation = useNavigation()

  return (
    <View style={styles.container}>
       
      <View >
        <Text style={styles.textPrimary} >Como deseas hacer tu calculo</Text>

      </View>
      
     
      {screens.map((screen, index) => (
        <TouchableOpacity
          key={index}
          style={styles.button}
          onPress={() => navigation.navigate(screen)}
        >
          <Text style={styles.buttonText}>{screen}</Text>
        </TouchableOpacity>
      ))}
      
      {/* <AdMobBanner
        bannerSize="banner"
        adUnitID="tu_ad_unit_id"
        servePersonalizedAds // opcional
        onDidFailToReceiveAdWithError={(error) => console.log(error)}
      />
      */}
     
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: '80%',
    height: 50,
    backgroundColor: 'purple',
    borderRadius: 10,
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
  },
  textPrimary:{
    fontSize : 25
  }
  
})

