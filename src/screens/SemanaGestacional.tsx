import React, {useState} from 'react'
import {  StyleSheet,Text,  View,  Button} from 'react-native'
import DateTimePickerModal from "react-native-modal-datetime-picker";


export const SemanaGestacional = () => {
  const [fechaInicial, setFechaInicial] = useState(false);
  const [datosGestacionales, setDatosGestacionales] = useState({diasTotal: 0, semanas:0, diasSemana:0,  fechaPosible:""})
  const fechaHoy = new Date()

  
  const showDatePicker = () => {
    setFechaInicial(true);
  };

  const hideDatePicker = () => {
    setFechaInicial(false);
  };

  const parteEnteraDecimal =(resul) =>{
    const data = {
      entero:0, decimal:0
    }
    
    data.entero= Math.floor(resul/7);
    data.decimal = Math.floor((resul/7 -  data.entero)*7);

    return data
  }

  const fechaPosibleParto = (date) =>{
    let parto = date.setMonth(date.getMonth() + 9)
    let fecha = new Date(parto);
    return fecha.toISOString().slice(0,10)
  }
  const handleConfirm = (date) => {
    let resta = fechaHoy.getTime() - date.getTime()
    let resul=Math.round(resta/ (1000*60*60*24))
     const {entero, decimal} = parteEnteraDecimal(resul)

    const fecha = fechaPosibleParto(date)
    
    setDatosGestacionales({...datosGestacionales, diasTotal: resul, semanas: entero, fechaPosible:fecha, diasSemana:decimal })
    hideDatePicker();
  };
 

  return (
    <View style={{
      flex: 1,
      alignItems: 'center',
      top:110,
      margin: 1,
  }}>

    <View style={{marginBottom: 15, width: "70%"}}>
          <Button title="Elige la fecha de la ultima mestruacion" onPress={showDatePicker} />
          <DateTimePickerModal
            isVisible={fechaInicial}
            mode="date"
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
          />
        </View>

        <View >
          <Text style={styles.resultados}>Han transicurrido un total de <Text style={{ fontWeight : "bold"}}>{datosGestacionales?.diasTotal} </Text>dias</Text>
          <Text style={styles.resultados}><Text style={{ fontWeight : "bold"}}>{datosGestacionales?.semanas}</Text>  semanas y <Text style={{ fontWeight : "bold"}}>{datosGestacionales?.diasSemana} </Text>dias </Text>
          <Text style={styles.resultados}>la fecha posible para el parto es {'\n'} <Text style={{ fontWeight : "bold", flex:1}}>{datosGestacionales?.fechaPosible}</Text></Text>
        </View>
     </View>

  )
}


const styles = StyleSheet.create({
  resultados:{
    fontSize: 18,
    marginTop: 10
    // fontWeight: 'bold',
  }
})