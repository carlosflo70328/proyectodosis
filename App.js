import { NavigationContainer } from '@react-navigation/native'
import { StyleSheet, Text, View } from 'react-native';
import { Navigation } from './src/Navigation';


export default function App() {
  return (
<NavigationContainer>
<Navigation/>
    </NavigationContainer>
    
    // <View style={styles.container}>
      // <Text>Open up App.js to start working on your app!</Text>
      // <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
